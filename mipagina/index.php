<!Doctype html>
<html>
	<head>
		<title>Mi primer pagina</title>
		<meta charset="utf-8"/>
	</head>
	<body>
		Hola a todos acabo de hacer un nuevo cambio		
		<div id="tipografia"> <!-- Es un agrupador -->
			<!-- Esta es la tipografía (Encabezados) -->
			<h1>Hola a todos</h1>
			<h2>Hola a todos</h2>
			<h3>Hola a todos</h3>
			<h4>Hola a todos</h4>
			<h5>Hola a todos</h5>
			<h6>Hola a todos</h6>
		</div>
		<hr/>
		<div id="parrafo"> <!-- agrupador -->
			<!-- así ponemos los párrafos -->
			<p align="left"> <!-- alineación left, center, right -->
				Esto que estoy poniendo <br/>
				aquí es un párrafo y no mantengo el formato <br/>
				porque no me da la gana.
			</p>
			<pre>Esto que estoy poniendo
aquí es un párrafo y no mantengo el formato
porque no me da la gana.</pre>
		</div>
		<hr/>
		<div id="urls"> <!-- enlaces a otras páginas -->
			<!--url a google -->
			<a href="http://google.com">Ir a google</a> 
		</div>
		<hr/>
		<div id="listasOrdenadas">
			Listas ordenadas<!-- son como viñetas enumeradas -->
			<ol>
				<li>Uno</li>
				<li>Dos</li>
				<li>Tres</li>
				<ul>
					<li>Tres .5</li>
				</ul>
			</ol>
		</div>
		<div id="listasDesordenadas">
			Listas desordenadas
			<ul>
				<li>Uno</li>
				<li>Dos</li>
				<li>Tres</li>
				<ol>
					<li>Tres .5</li>
				</ol>
			</ul>
		</div>
		<hr/>
		<div id="imagenes" style="text-align:center">
			<!-- imagen alineada a la izquierda -->
			<img src="imagenes/1.jpg" alt="1" 
					width="200" height="200" align="left">
			<!-- imagen alineada a la derecha -->
			<img src="imagenes/2.jpg" alt="2" 
					width="200" height="200" align="right">
			<!-- para centrarla se ocupa indicar en el DIV: 
				<div style="text-align:center">
			-->
			<img src="imagenes/3.jpg" alt="3" 
					width="200" height="200">
		</div>
		<hr/>
		<div id="tablas">
			<table border="1px">
				<caption>Listado de personal de Sistemas</caption>
				<thead>
					<th>Depto.</th>
					<th>No.</th>
					<th>Nombre</th>
					<th>Apellidos</th>
					<th>Edad</th>
				</thead>
				<tbody>
					<tr>
						<td rowspan="4">Sistemas</td>
						<td>1</td>
						<td>Roberto</td>
						<td>Zamarripa</td>
						<td>15</td>
					</tr>
					<tr>
						<td>2</td>
						<td>Juan Carlos</td>
						<td>Cotera García</td>
						<td>26</td>
					</tr>
					<tr>
						<td>3</td>
						<td>Jeaneth Isamar</td>
						<td>Montoya Romo</td>
						<td>23</td>
					</tr>
					<tr>						
						<td colspan="3">Total</td>
						<td>64</td>
					</tr>
				</tbody>
			</table>
		</div>
		<hr/>
		<form action="otrapagina.php" method="POST">
			<div id="formularios">
				<label for="nombre">Nombre</label>
				<input id="nombre" type="text" 
						style="width:400px" name="txtNombre"
						placeholder="Escriba su nombre" >
				<br/>
				<input id="chkPersona" type="checkbox" name="chkPersona">
				<label for="chkPersona">Eres Persona?</label>
				<input id="chkExtraterrestre" type="checkbox" name="chkExtraterrestre">
				<label for="chkExtraterrestre">Eres Extraterrestre?</label>			
				<br/>
				<label for="rbAsistio">Asistió</label>
				<input id="rbAsistio" type="radio" name="rbAsistio" value="1">
				<label for="rbAsistio">No Asistió</label>
				<input id="rbAsistio" type="radio" name="rbAsistio" value="2">
				<br/>
				<label for="pais">País</label>
				<select id="pais" name="pais">
					<option value="Mexico">México</option>
					<option value="Estados Unidos">Estados Unidos</option>
					<option value="Marte">Marte</option>
					<option value="Otro">Otro</option>
				</select>
				<br/>
				<label for="comentario">Comentario</label>
				<br/>
				<textarea name="comentario" id="comentario" rows="3" cols="50" ></textarea>
				<br/>
				<button type="submit">Enviar</button>			
			</div>
		<form>
		<hr/>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	</body>
</html>
<!-- Esto es la estructura básica html -->









