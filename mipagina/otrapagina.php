<?php	
	$persona = $_POST; ?>
<!Doctype html>
<html>
	<head>
		<title>Mi primer pagina</title>
		<meta charset="utf-8"/>
		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<table class="table table-striped table-bordered span6 offset2">
					<caption><h4>Listado de participantes</h4></caption>
					<thead class="thead">
						<tr>
							<th>Acción</th>
							<th>Nombre</th>
							<th>Tipo de Ser</th>
							<th>Estatus</th>
							<th>País</th>
							<th>Comentario</th>
						</tr>
					</thead>
					<tbody>		
						<tr>
							<td><a href="index.php" class="btn btn-info">Volver</a></td>	
							<td><?php echo "Bienvenido " . $persona["txtNombre"];?></td>	
							<td><?php if(isset($persona["chkPersona"]))
											echo "Persona";
										else
											echo "Marciano";
							?></td>			
							<td><?php if($persona["rbAsistio"]== 1)
											echo "Asistió";
										else
											echo "No Asistió";				
							?></td>
							<td><?php echo $persona["pais"];?></td>			
							<td><?php echo $persona["comentario"];?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>