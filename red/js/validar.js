function revisarInicioSesion(){
	if(formularioSesion.usuario.value == ""){
		formularioSesion.usuario.setAttribute("class", "input-xlarge error");		
		alert("El usuario no puede estar en blanco");		
		return false;
	} else {
		formularioSesion.usuario.setAttribute("class", "input-xlarge valido");		
	}
	if(formularioSesion.contrasena.value == ""){
		alert("La contraseña no puede estar en blanco");
		formularioSesion.contrasena.setAttribute("class", "input-xlarge error");
		return false;
	} else {
		formularioSesion.contrasena.setAttribute("class", "input-xlarge valido");
	}
}